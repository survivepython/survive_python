from classes.Survivor import Survivor
from database.database_setup import database_setup


def main():
    database_setup()

    survivor = Survivor()
    survivor.get(1)
    survivor.regen_health()
    survivor.regen_stamina()
    survivor.regen_will()
    print(survivor.get_details(True))
    survivor.save()


if __name__ == '__main__':
    main()
