resources = {
    '0': {
        'name': 'Wood',
        'description': 'Wood',
        'weight': 10,
        'volume': 20,
    },
    '1': {
        'name': 'Stone',
        'description': 'Stone',
        'weight': 20,
        'volume': 5,
    },
    '2': {
        'name': 'Iron',
        'description': 'Iron',
        'weight': 20,
        'volume': 2,
    },
    '3': {
        'name': 'Ammunition',
        'description': 'Ammunition',
        'weight': 0.1,
        'volume': 0.1,
    },
    '4': {
        'name': 'Medicine',
        'description': 'Medicine',
        'health': 50,
        'stamina': 50,
        'weight': 0.1,
        'volume': 0.1,
    },
    '5': {
        'name': 'Cement',
        'description': 'Cement',
        'weight': 5,
        'volume': 0.5,
    },
    '6': {
        'name': 'Rubber',
        'description': 'Rubber',
        'weight': 2,
        'volume': 2,
    },
}
