import subprocess

subprocess.call(["pip3", "install", "Django>=2.1.2,<2.2.0"])

subprocess.call(["pip3", "install", "unittest-data-provider>=1.0.1,<1.1.0"])

subprocess.call(["pip3", "install", "django-crispy-forms>=1.7.2,<1.8.0"])
