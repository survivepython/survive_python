# Server Setup

#### 1. Machine Setup
##### 1.1. Install Git
1. Go to [git-scm.com](https://git-scm.com/downloads)
2. Download & install the latest version of Git
##### 1.2. Install Python
1. Go to [www.python.org](https://www.python.org/downloads/release/python-371/)
2. Download the latest 3.7 python version
3. Install Python on your machine, make sure you check ["Add Python 3.7 to PATH"](https://i.stack.imgur.com/CCXQG.jpg)

#### 2. Download Code Base
1. Access the [Survive repository](https://bitbucket.org/survivepython/survive/src/master/) (make sure you are on the **master** branch)
2. Click on **Clone** (the button should be in the upper-right corner - you must be logged in)
3. Select **HTTPS** (from the SSH/HTTPS drop-down located in the upper-right corner of the pop-up)
4. **Copy** the given **git clone command** ("git clone git@bitbucket.org:survivepython/survive.git")
5. On your machine/computer, open GitBash (git terminal)
6. Navigate to where you'd like to save the project
7. Paste the git clone command into the terminal and hit enter
###### You now have the latest version of the survive codebase on your local machine, congratulations!

#### 3. Server Setup
1. On your machine/computer, open GitBash (git terminal)
2. Navigate into the survive folder
3. Run the following command: **python setup_server.py**
###### The server is now ready, all we need to do is start it

#### 4. Start Server
1. On your machine/computer, open GitBash (git terminal)
2. Navigate into the survive folder
3. Run the following command: **python run_server.py**
4. In the browser, open [localhost:8000](http://localhost:8000)
###### The server is now up and running, weeeeee....


# You can ignore what is written bellow
# Django
## Create new project
django-admin startproject [project_name]

## Starting the server
python [path_to_project]/manage.py runserver
- on [http://localhost:8000/](http://localhost:8000/)

## Create new app
python [path_to_project]/manage.py startapp [app_name]

## Create Django Migrations based on the models that we added/changed
python [path_to_project]/manage.py makemigrations

## Run Django Migrations
python [path_to_project]/manage.py migrate

## Create a superuser for Admin
python [path_to_project]/manage.py createsuperuser

## Print the sql from a migration file
python [path_to_project]/manage.py sqlmigrate [app_name] [migration_number]
ex: python manage.py sqlmigrate blog 0001
