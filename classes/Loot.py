import random

from classes.Database import Database
from functions import separator_utils, error_utils


class Loot:
    def __init__(self):
        self.__database_table = 'loot'
        self.__id = None
        self.__name = None
        self.__description = None
        self.__health = None
        self.__stamina = None
        self.__weight = None
        self.__volume = None
        self.__type = None
        self.__possible_loot_types = ['tool', 'food', 'resource']
        self.__integrity = None
        self.__damage = None
        self.__range = None

    def create(self, name=None, description=None, health=None, stamina=None, weight=None, volume=None, loot_type=None, integrity=None, damage=None, loot_range=None):
        self.set_name(name)
        self.set_description(description)
        self.set_health(health)
        self.set_stamina(stamina)
        self.set_weight(weight)
        self.set_volume(volume)
        self.set_type(loot_type)
        self.set_integrity(integrity)
        self.set_damage(damage)
        self.set_range(loot_range)

    def get(self, identifier=None):
        db = Database()

        if identifier is None or type(identifier) not in [int, str]:
            error_utils.value_error('Loot.get() - identifier must be str or int type, got {0} {1}'.format(identifier, type(identifier)))

        if type(identifier) == int:
            where_clause = 'id = {0}'.format(identifier)
        else:
            where_clause = 'name = \'{0}\''.format(identifier)

        fields = ['*']

        order = None
        limit = 1
        distinct = None

        loot_data = db.select_table_data(self.__database_table, fields, where_clause, order, limit, distinct, False)

        if loot_data:
            self.__id = loot_data[0]
            self.__name = loot_data[1]
            self.__description = loot_data[2]
            self.__health = loot_data[3]
            self.__stamina = loot_data[4]
            self.__weight = loot_data[5]
            self.__volume = loot_data[6]
            self.__type = loot_data[7]
            self.__integrity = loot_data[8]
            self.__damage = loot_data[9]
            self.__range = loot_data[10]

            return True
        else:
            return False

    def save(self):
        db = Database()

        fields = []
        values = []
        loot_id = self.__id

        if loot_id:
            fields = ['id']
            values = [loot_id]

        fields.extend(['name', 'description', 'health', 'stamina', 'weight', 'volume', 'type', 'integrity', 'damage', 'range'])

        name = self.__name
        description = self.__description
        health = self.__health
        stamina = self.__stamina
        weight = self.__weight
        volume = self.__volume
        loot_type = self.__type
        integrity = self.__integrity
        damage = self.__damage
        loot_range = self.__range

        values.extend([name, description, health, stamina, weight, volume, loot_type, integrity, damage, loot_range])

        if loot_id:
            where_clause = 'id = {}'.format(loot_id)
            db.update_table_data(self.__database_table, fields, values, where_clause)
        else:
            db.insert_into_table(self.__database_table, fields, values)

        if not self.__id:
            self.__get_id_of_latest_loot()

    def __get_id_of_latest_loot(self):
        db = Database()

        fields = ['id']
        where_clause = None
        order = 'id DESC'
        limit = 1
        distinct = None

        loot_data = db.select_table_data(self.__database_table, fields, where_clause, order, limit, distinct, False)

        self.__id = loot_data

    def get_id(self):
        return self.__id

    def set_name(self, name=None):
        if type(name) == str:
            self.__name = name
            return

        error_utils.value_error('Loot.set_name() - name must be str type, got {0} {1}'.format(name, type(name)))

    def get_name(self):
        return self.__name

    def set_type(self, loot_type=None):
        possible_loot_types = self.__possible_loot_types

        if loot_type is None:
            self.__type = random.choice(possible_loot_types)
            return

        if loot_type in possible_loot_types:
            self.__type = loot_type
            return

        if type(loot_type) == int and 0 <= loot_type <= len(possible_loot_types):
            self.__type = possible_loot_types[loot_type]
            return

        error_utils.value_error('Loot.set_type() - type must be a str type in {0}, got {1} {2}'.format(possible_loot_types, loot_type, type(loot_type)))

    def get_type(self):
        return self.__type

    def set_description(self, description=None):
        if type(description) == str:
            self.__description = description
            return

        error_utils.value_error('Loot.set_description() - description must be a str type, got {} {}'.format(description, type(description)))

    def get_description(self):
        return self.__description

    def set_health(self, health=None):
        if health is None:
            self.__health = 0
            return

        if type(health) == int:
            self.__health = health
            return

        error_utils.value_error('Loot.set_health() - health must be int type, got {}{}'.format(health, type(health)))

    def modify_health(self, health=None):
        if health is None or type(health) != int:
            error_utils.value_error('Loot.modify_health() - health must be int type, got {}{}'.format(health, type(health)))

        health = self.__health + health

        self.set_health(health)

    def get_health(self):
        return self.__health

    def set_stamina(self, stamina=None):
        if stamina is None:
            self.__stamina = 0
            return

        if type(stamina) == int:
            self.__stamina = stamina
            return

        error_utils.value_error('Loot.set_stamina() - stamina must be int type, got {}{}'.format(stamina, type(stamina)))

    def modify_stamina(self, stamina=None):
        if stamina is None or type(stamina) != int:
            error_utils.value_error('Loot.modify_stamina() - stamina must be int type, got {}{}'.format(stamina, type(stamina)))

        stamina = self.__stamina + stamina

        self.set_stamina(stamina)

    def get_stamina(self):
        return self.__stamina

    def set_weight(self, weight=None):
        if type(weight) == int:
            self.__weight = weight
            return

        error_utils.value_error('Loot.set_weight() - weight must be int type, got {}{}'.format(weight, type(weight)))

    def modify_weight(self, weight=None):
        if weight is None or type(weight) != int:
            error_utils.value_error('Loot.modify_weight() - weight must be int type, got {}{}'.format(weight, type(weight)))

        weight = self.__weight + weight

        self.set_weight(weight)

    def get_weight(self):
        return self.__weight

    def set_volume(self, volume=None):
        if type(volume) == int:
            self.__volume = volume
            return

        error_utils.value_error('Loot.set_volume() - volume must be int type, got {}{}'.format(volume, type(volume)))

    def modify_volume(self, volume=None):
        if volume is None or type(volume) != int:
            error_utils.value_error('Loot.modify_volume() - volume must be int type, got {}{}'.format(volume, type(volume)))

        volume = self.__volume + volume

        self.set_volume(volume)

    def get_volume(self):
        return self.__volume

    def set_integrity(self, integrity=None):
        if type(integrity) == int:
            self.__integrity = integrity
            return

        error_utils.value_error('Loot.get_volume() - integrity must be int type, got {}{}'.format(integrity, type(integrity)))

    def modify_integrity(self, integrity=None):
        if integrity is None or type(integrity) != int:
            error_utils.value_error('Loot.modify_integrity() - integrity must be int type, got {}{}'.format(integrity, type(integrity)))

        integrity = self.__integrity + integrity

        self.set_integrity(integrity)

    def get_integrity(self):
        return self.__integrity

    def set_damage(self, damage=None):
        if type(damage) == int:
            self.__damage = damage
            return

        error_utils.value_error('Loot.set_damage() - damage must be int type, got {}{}'.format(damage, type(damage)))

    def modify_damage(self, damage=None):
        if damage is not None and type(damage) != int:
            error_utils.value_error('Loot.modify_damage() - damage must be int type, got {}{}'.format(damage, type(damage)))

        damage = self.__damage + damage

        self.set_damage(damage)

    def get_damage(self):
        return self.__damage

    def set_range(self, loot_range=None):
        if type(loot_range) == int:
            self.__range = loot_range
            return

        error_utils.value_error('Loot.set_range() - range must be int type, got {}{}'.format(loot_range, type(loot_range)))

    def modify_range(self, loot_range=None):
        if loot_range is None or type(loot_range) != int:
            error_utils.value_error('Loot.modify_range() - damage must be int type, got {}{}'.format(loot_range, type(loot_range)))

        loot_range = self.__range + loot_range

        self.set_range(loot_range)

    def get_range(self):
        return self.__range

    def get_details(self, print_data=False):
        loot_id = self.__id
        name = self.__name
        description = self.__description
        health = self.__health
        stamina = self.__stamina
        weight = self.__weight
        volume = self.__volume
        loot_type = self.__type
        integrity = self.__integrity
        damage = self.__damage
        loot_range = self.__range

        if print_data:
            separator_utils.print_separator()
            print('ID         : {}'.format(loot_id))
            print('Name       : {}'.format(name))
            print('Description: {}'.format(description))
            print('Health     : {}'.format(health))
            print('Stamina    : {}'.format(stamina))
            print('Weight     : {}'.format(weight))
            print('Volume     : {}'.format(volume))
            print('Type       : {}'.format(loot_type))
            print('Integrity  : {}'.format(integrity))
            print('Damage     : {}'.format(damage))
            print('Range      : {}'.format(loot_range))
            separator_utils.print_separator()

        return loot_id, name, description, health, stamina, weight, volume, health, loot_type, integrity, damage, loot_range
