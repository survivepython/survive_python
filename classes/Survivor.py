import random

from assets.names import unisex_nicknames, female_nicknames, male_nicknames
from classes.Database import Database
from functions import separator_utils, error_utils


class Survivor:
    def __init__(self):
        self.__database_table = 'survivors'
        # Identity
        self.__id = None
        self.__nickname = None
        self.__sex = None
        self.__possible_sexes = ['female', 'male']
        self.__age = None
        self.__age_range = [1, 100]
        # Experience
        self.__level = None
        self.__experience = 0
        self.__experience_required = 100
        self.__experience_increase = .2  # percentage increase per level
        # Health & Energy
        self.__health = None
        self.__health_max = 100
        self.__health_regen = 1
        self.__stamina = None
        self.__stamina_max = 100
        self.__stamina_regen = 1
        self.__will = None
        self.__will_max = 100
        self.__will_regen = 1

    def create(self, nickname=None, sex=None, age=None, level=None, experience=None, health=None, stamina=None, will=None):
        self.set_sex(sex)
        self.set_nickname(nickname)
        self.set_age(age)
        self.set_level(level)
        self.set_experience(experience)
        self.set_health(health)
        self.set_stamina(stamina)
        self.set_will(will)

    def get(self, identifier=None):
        db = Database()

        if identifier is None or type(identifier) not in [int, str]:
            error_utils.value_error('Survivor identifier must be str or int type, got {0} {1}'.format(identifier, type(identifier)))

        if type(identifier) == int:
            where_clause = 'id = {0}'.format(identifier)
        else:
            where_clause = 'nickname = \'{0}\''.format(identifier)

        fields = ['*']

        order = None
        limit = 1
        distinct = None

        survivor_data = db.select_table_data(self.__database_table, fields, where_clause, order, limit, distinct, False)

        if survivor_data:
            self.__id = survivor_data[0]
            self.__nickname = survivor_data[1]
            self.__sex = survivor_data[2]
            self.__age = survivor_data[3]
            self.__level = survivor_data[4]
            self.__experience = survivor_data[5]
            self.__experience_required = survivor_data[6]
            self.__health = survivor_data[7]
            self.__health_max = survivor_data[8]
            self.__health_regen = survivor_data[9]
            self.__stamina = survivor_data[10]
            self.__stamina_max = survivor_data[11]
            self.__stamina_regen = survivor_data[12]
            self.__will = survivor_data[13]
            self.__will_max = survivor_data[14]
            self.__will_regen = survivor_data[15]

            return True
        else:
            return False

    def save(self):
        db = Database()

        fields = []
        values = []
        survivor_id = self.__id

        if survivor_id:
            fields = ['id']
            values = [survivor_id]

        fields.extend(['nickname', 'sex', 'age', 'level', 'experience', 'experience_required', 'health', 'health_max', 'health_regen', 'stamina', 'stamina_max', 'stamina_regen', 'will', 'will_max',
                       'will_regen'])

        nickname = self.__nickname
        sex = self.__sex
        age = self.__age
        level = self.__level
        experience = self.__experience
        experience_required = self.__experience_required
        health = self.__health
        health_max = self.__health_max
        health_regen = self.__health_regen
        stamina = self.__stamina
        stamina_max = self.__stamina_max
        stamina_regen = self.__stamina_regen
        will = self.__will
        will_max = self.__will_max
        will_regen = self.__will_regen

        values.extend([nickname, sex, age, level, experience, experience_required, health, health_max, health_regen, stamina, stamina_max, stamina_regen, will, will_max, will_regen])

        if survivor_id:
            where_clause = 'id = {}'.format(survivor_id)
            db.update_table_data(self.__database_table, fields, values, where_clause)
        else:
            db.insert_into_table(self.__database_table, fields, values)

        if not self.__id:
            self.__get_id_of_latest_survivor()

    def __get_id_of_latest_survivor(self):
        # Setup database connection
        db = Database()

        fields = ['id']
        where_clause = None
        order = 'id DESC'
        limit = 1
        distinct = None

        survivor_data = db.select_table_data(self.__database_table, fields, where_clause, order, limit, distinct, False)

        self.__id = survivor_data

    def get_id(self):
        return self.__id

    def set_nickname(self, nickname=None):
        if nickname is None:
            self.__generate_nickname()
            return

        if type(nickname) == str:
            self.__nickname = nickname
            return

        error_utils.value_error('Survivor.set_nickname - nickname must be str type, got {0} {1}'.format(nickname, type(nickname)))

    def __generate_nickname(self):
        nicknames = unisex_nicknames

        if self.__sex is None:
            error_utils.value_error('Survivor.__generate_nickname - sex must be set in order to generate nickname')

        if self.__sex == 'female':
            nicknames.extend(female_nicknames)
        else:
            nicknames.extend(male_nicknames)

        self.__nickname = random.choice(nicknames)

    def get_nickname(self):
        return self.__nickname

    def set_sex(self, sex=None):
        possible_sexes = self.__possible_sexes

        if sex is None:
            self.__sex = random.choice(possible_sexes)
            return

        if sex in possible_sexes:
            self.__sex = sex
            return

        if type(sex) == int and 0 <= sex <= len(possible_sexes):
            self.__sex = possible_sexes[sex]
            return

        error_utils.value_error('Survivor.set_sex() -  sex must be a str type in or index of {0}, got {1} {2}'.format(possible_sexes, sex, type(sex)))

    def get_sex(self):
        return self.__sex

    def set_age(self, age=None):
        minimum = self.__age_range[0]
        maximum = self.__age_range[1]

        if age is None:
            age_group = random.randint(1, 100)

            if age_group <= 5:
                # Baby - 5%
                age = random.randint(1, 4)
            elif age_group <= 15:
                # Child - 10%
                age = random.randint(5, 12)  # up to 12
            elif age_group <= 35:
                # Teenager - 20%
                age = random.randint(13, 19)
            elif age_group <= 65:
                # Young Adult - 30%
                age = random.randint(20, 29)
            elif age_group <= 85:
                # Adult - 20%
                age = random.randint(30, 59)
            elif age_group <= 95:
                # Senior - 10%
                age = random.randint(60, 79)
            else:
                # Elder - 5%
                age = random.randint(80, 100)

            self.__age = age
            return

        if type(age) == int and minimum <= age <= maximum:
            self.__age = age
            return

        error_utils.value_error('Survivor.set_age() - age must be an int type in range of {0}-{1}, got {2} {3}'.format(minimum, maximum, age, type(age)))

    def get_age(self):
        return self.__age

    def increase_age(self):
        self.__age += 1

    def set_level(self, level=None):
        if level is None:
            self.__level = 1
            return

        if type(level) == int and level > 0:
            self.__level = level
            return

        error_utils.value_error('Survivor.set_level() - level must be an int > 0, got {0} {1}'.format(level, type(level)))

    def get_level(self):
        return self.__level

    def increase_level(self):
        self.__level += 1

    def set_experience(self, experience=None):

        if self.__level is None:
            error_utils.value_error('Survivor.set_experience() - level must be set in order to set experience')

        if experience is None:
            self.__experience = 0
            return

        if type(experience) == int:
            self.__calculate_level_for_provided_experience(experience)
            return

        error_utils.value_error('Survivor.set_experience() - experience must be an int >= 0, got {0} {1}'.format(experience, type(experience)))

    def __calculate_level_for_provided_experience(self, experience=None):
        if experience is None or type(experience) != int:
            error_utils.value_error('Survivor.__calculate_level_for_provided_experience() - experience is required and must be int type in order to calculate required experience')

        experience_required = self.__calculate_experience_required_for_level()

        if experience < experience_required:
            self.__experience = experience
            return

        if experience >= experience_required:
            self.__level += 1
            experience = experience - experience_required
            self.__calculate_level_for_provided_experience(experience)

    def __calculate_experience_required_for_level(self):
        level = self.__level
        experience_required = self.__experience_required
        experience_increase = self.__experience_increase

        if level == 1:
            return experience_required

        for i in range(1, level):
            experience_required += experience_required + (experience_required * experience_increase)

        experience_required = int(experience_required)

        self.__experience_required = experience_required

        return experience_required

    def get_experience(self):
        return self.__experience

    def modify_experience(self, experience):
        if experience is None or type(experience) != int:
            error_utils.value_error('Survivor.modify_experience() - experience is required and must be int type in order to increase experience')

        self.set_experience(self.__experience + experience)

    def set_health(self, health=None):
        health_max = self.__health_max
        if health is None:
            self.__health = health_max
            return

        if type(health) == int:
            if health <= 0:
                self.__health = 0
                return

            if health < health_max:
                self.__health = health
                return

            self.__health = health_max
            return

        error_utils.value_error('Survivor.set_health() - health must be int type, got {}{}'.format(health, type(health)))

    def modify_health(self, health=None):
        if health is None:
            error_utils.value_error('Survivor.modify_health() - health must be int type, got {}{}'.format(health, type(health)))

        if health is not None and type(health) != int:
            error_utils.value_error('Survivor.modify_health() - health must be int type, got {}{}'.format(health, type(health)))

        health = self.__health + health

        self.set_health(health)

    def regen_health(self, health=None):
        if health is not None and type(health) != int:
            error_utils.value_error('Survivor.regen_health() - health must be int type, got {}{}'.format(health, type(health)))

        if health is None:
            health = self.__health_regen

        if health > 0:
            if self.__health != self.__health_max:
                if (self.__health + health) <= self.__health_max:
                    self.__health += health
                else:
                    self.__health += self.__health_max
        else:
            if self.__health > 0:
                if (self.__health + health) >= 0:
                    self.__health += health
                else:
                    self.__health = 0

    def get_health(self):
        return self.__health

    def set_stamina(self, stamina=None):
        stamina_max = self.__stamina_max
        if stamina is None:
            self.__stamina = stamina_max
            return

        if type(stamina) == int:
            if stamina <= 0:
                self.__stamina = 0
                return

            if stamina < stamina_max:
                self.__stamina = stamina
                return

            self.__stamina = stamina_max
            return

        error_utils.value_error('Survivor.set_stamina() - stamina must be int type, got {}{}'.format(stamina, type(stamina)))

    def modify_stamina(self, stamina=None):
        if stamina is None:
            error_utils.value_error('Survivor.modify_stamina() - stamina must be int type, got {}{}'.format(stamina, type(stamina)))

        if stamina is not None and type(stamina) != int:
            error_utils.value_error('Survivor.modify_stamina() - stamina must be int type, got {}{}'.format(stamina, type(stamina)))

        stamina = self.__stamina + stamina

        self.set_stamina(stamina)

    def regen_stamina(self, stamina=None):
        if stamina is not None and type(stamina) != int:
            error_utils.value_error('Survivor.regen_stamina() - stamina must be int type, got {}{}'.format(stamina, type(stamina)))

        if stamina is None:
            stamina = self.__stamina_regen

        if stamina > 0:
            if self.__stamina != self.__stamina_max:
                if (self.__stamina + stamina) <= self.__stamina_max:
                    self.__stamina += stamina
                else:
                    self.__stamina += self.__stamina_max
        else:
            if self.__stamina > 0:
                if (self.__stamina + stamina) >= 0:
                    self.__stamina += stamina
                else:
                    self.__stamina = 0

    def get_stamina(self):
        return self.__stamina

    def set_will(self, will=None):
        will_max = self.__will_max
        if will is None:
            self.__will = will_max
            return

        if type(will) == int:
            if will <= 0:
                self.__will = 0
                return

            if will < will_max:
                self.__will = will
                return

            self.__will = will_max
            return

        error_utils.value_error('Survivor.set_will() - will must be int type, got {}{}'.format(will, type(will)))

    def modify_will(self, will=None):
        if will is None:
            error_utils.value_error('Survivor.modify_will() - will must be int type, got {}{}'.format(will, type(will)))

        if will is not None and type(will) != int:
            error_utils.value_error('Survivor.modify_will() - will must be int type, got {}{}'.format(will, type(will)))

        will = self.__will + will

        self.set_will(will)

    def regen_will(self, will=None):
        if will is not None and type(will) != int:
            error_utils.value_error('Survivor.regen_will() - will must be int type, got {}{}'.format(will, type(will)))

        if will is None:
            will = self.__will_regen

        if will > 0:
            if self.__will != self.__will_max:
                if (self.__will + will) <= self.__will_max:
                    self.__will += will
                else:
                    self.__will += self.__will_max
        else:
            if self.__will > 0:
                if (self.__will + will) >= 0:
                    self.__will += will
                else:
                    self.__will = 0

    def get_will(self):
        return self.__will

    def get_details(self, print_data=False):
        survivor_id = self.__id
        nickname = self.__nickname
        sex = self.__sex
        age = self.__age
        level = self.__level
        experience = self.__experience
        experience_required = self.__experience_required
        health = self.__health
        health_max = self.__health_max
        health_regen = self.__health_regen
        stamina = self.__stamina
        stamina_max = self.__stamina_max
        stamina_regen = self.__stamina_regen
        will = self.__will
        will_max = self.__will_max
        will_regen = self.__will_regen

        if print_data:
            separator_utils.print_separator()
            print('ID        : {}'.format(survivor_id))
            print('Nickname  : {}'.format(nickname))
            print('Sex       : {}'.format(sex))
            print('Age       : {}'.format(age))
            print('Level     : {}'.format(level))
            print('Experience: {}/{}'.format(experience, experience_required))
            print('Health    : {}/{} - regen: {}'.format(health, health_max, health_regen))
            print('Stamina   : {}/{} - regen: {}'.format(stamina, stamina_max, stamina_regen))
            print('Will      : {}/{} - regen: {}'.format(will, will_max, will_regen))
            separator_utils.print_separator()

        return survivor_id, nickname, sex, age, level, experience, experience_required, health, health_max, health_regen, stamina, stamina_max, stamina_regen, will, will_max, will_regen
