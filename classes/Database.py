import os
import sqlite3
import sys

# Get root directory
import config

root_directory = config.ROOT_DIR


class Database:
    def __init__(self):
        # Create database folder if it does not exist
        if not os.path.exists(root_directory + '/database'):
            os.makedirs(root_directory + '/database')

        self.database_location = root_directory + '/database/SQLite_database.sqlite'
        self.conn, self.c = self.connect()

    def connect(self):
        """ Make connection to an SQLite database file """
        conn = sqlite3.connect(self.database_location)
        c = conn.cursor()
        return conn, c

    def disconnect(self):
        """ Commit changes and close connection to the database """
        self.conn.commit()
        self.conn.close()

    def create_table(self, table_name, new_fields, print_out=False):

        # Start creating the sql string
        sql = 'CREATE TABLE IF NOT EXISTS {0} ('.format(table_name)

        # Add new columns to sql string
        number_of_fields = len(new_fields)
        fields_count = 0
        for new_field in new_fields:
            # Add column & column properties to sql
            data_count = 0
            for field_data in new_field:
                sql += '{0}'.format(field_data)

                # Add "," to sql if it is the last field or " " if it is not
                count_field_data = len(new_field)
                if data_count == (count_field_data - 1):
                    if fields_count == (number_of_fields - 1):
                        sql += ''
                    else:
                        sql += ', '
                else:
                    sql += ' '

                    data_count += 1
            fields_count += 1

        # Close the parenthesis of the sql fields
        sql += ')'

        # Print query if requested
        if print_out:
            print(sql)

        self.c.execute(sql)
        self.conn.commit()

    def insert_into_table(self, table_name, fields, values, print_out=False, ignore_if_error=False):

        self.check_number_of_fields_vs_values(fields, values)

        if ignore_if_error is True:
            ignore = 'OR IGNORE '
        else:
            ignore = ''

        # Start creating the sql string
        sql = 'INSERT {}INTO {} ('.format(ignore, table_name)

        # Add fields to sql string
        count_fields = len(fields)
        count = 1
        for field in fields:
            # Add column to sql
            sql += '{0}'.format(field)

            # Add "," after all but the last fields
            if count != count_fields:
                sql += ', '

            count += 1

        # Close fields parenthesis
        sql += ') VALUES ('

        # Add values to sql string
        count_fields = len(values)
        count = 1
        for value in values:
            # Add value to sql
            if type(value) == str:
                sql += '\'{0}\''.format(value)
            else:
                sql += '{0}'.format(value)

            # Add "," after all but the last fields
            if count != count_fields:
                sql += ', '

            count += 1

        # Close the parenthesis of the sql fields
        sql += ')'

        # Print query if requested
        if print_out:
            print(sql)

        self.c.execute(sql)
        self.conn.commit()

    def update_table_data(self, table_name, fields, values, where_clause=None, print_out=False):

        self.check_number_of_fields_vs_values(fields, values)

        # Start creating the sql string
        sql = 'UPDATE {0} SET '.format(table_name)

        # Add fields to sql string
        for i in range(0, len(fields)):
            # Add column to sql
            if type(values[i]) == str:
                sql += '{0} = \'{1}\''.format(fields[i], values[i])
            else:
                sql += '{0} = {1}'.format(fields[i], values[i])

            if i < len(fields) - 1:
                sql += ', '

        # Add where clause to sql
        sql += ' WHERE {0}'.format(where_clause)

        # Print query if requested
        if print_out:
            print(sql)

        self.c.execute(sql)
        self.conn.commit()

    def select_table_data(self, table_name, fields, where_clause=None, order=None, limit=None, distinct=None,
                          print_out=False):

        # Start creating the sql string
        sql = 'SELECT '

        # Add distinct clause if needed
        if distinct:
            sql += ' DISTINCT '

        # Add fields to sql string
        for i in range(0, len(fields)):
            # Add column to sql
            sql += '{0}'.format(fields[i])

            if i < len(fields) - 1:
                sql += ', '

        # Add from table
        sql += ' FROM {0}'.format(table_name)

        # Add where clause if needed
        if where_clause:
            sql += ' WHERE {0}'.format(where_clause)

        # Add order if needed
        if order:
            sql += ' ORDER by {0}'.format(order)

        # Add limit if needed
        if limit:
            sql += ' LIMIT {0}'.format(limit)

        # Print query if requested
        if print_out:
            print(sql)

        # Execute the query
        self.c.execute(sql)

        # Return the results
        if limit == 1:
            # Return only the value if a single field was set
            if len(fields) == 1 and fields != ['*']:
                return self.c.fetchone()[0]
            return self.c.fetchone()
        else:
            return self.c.fetchall()

    @staticmethod
    def check_number_of_fields_vs_values(fields, values):
        if len(fields) != len(values):
            sys.exit('Database Error: fields # != values # => \n fields: {0}\nvalues: {1}'.format(fields, values))
