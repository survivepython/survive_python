import time

default_sleep = 0.01


def value_error(message=None):
    time.sleep(default_sleep)

    if not message:
        message = ''

    raise ValueError(message)
