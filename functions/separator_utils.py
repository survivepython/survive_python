#!/usr/bin/env python

separator = '--------------------------------------------------------------------------------------'


# Prints a separator
def print_separator():
    print(separator)


# Prints a word with separators above and below it
def print_between_separators(text):
    print('{0}\n{1}\n{2}'.format(separator, text, separator))


# Prints a word before a separator
def print_before_separator(text):
    print('{0}\n{1}'.format(text, separator))


# Prints a word after a  separator
def print_after_separator(text):
    print('{0}\n{1}'.format(separator, text))
