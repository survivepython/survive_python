from assets.foods import foods
from assets.resources import resources
from assets.tools import tools
from classes.Database import Database


def database_setup():
    db = Database()

    survivors_table = 'survivors'
    create_survivors_table(db, survivors_table)

    survivor_inventory_table = 'survivor_inventory'
    create_survivors_inventory_table(db, survivor_inventory_table)

    loot_table = 'loot'
    create_loot_table(db, loot_table)
    insert_food_into_loot_table(db, loot_table)
    insert_tools_into_loot_table(db, loot_table)
    insert_resources_into_loot_table(db, loot_table)


def create_survivors_table(db, table):
    new_table_name = table

    new_table_fields = [
        [
            'id',
            'INTEGER',
            'PRIMARY KEY',
            'AUTOINCREMENT',
        ],
        [
            'nickname',
            'TEXT',
            'NOT NULL',
        ],
        [
            'sex',
            'TEXT',
            'NOT NULL',
        ],
        [
            'age',
            'INTEGER',
            'NOT NULL',
        ],
        [
            'level',
            'INTEGER',
            'NOT NULL',
        ],
        [
            'experience',
            'INTEGER',
            'NOT NULL',
        ],
        [
            'experience_required',
            'INTEGER',
            'NOT NULL',
        ],
        [
            'health',
            'INTEGER',
            'NOT NULL',
        ],
        [
            'health_max',
            'INTEGER',
            'NOT NULL',
        ],
        [
            'health_regen',
            'INTEGER',
            'NOT NULL',
        ],
        [
            'stamina',
            'INTEGER',
            'NOT NULL',
        ],
        [
            'stamina_max',
            'INTEGER',
            'NOT NULL',
        ],
        [
            'stamina_regen',
            'INTEGER',
            'NOT NULL',
        ],
        [
            'will',
            'INTEGER',
            'NOT NULL',
        ],
        [
            'will_max',
            'INTEGER',
            'NOT NULL',
        ],
        [
            'will_regen',
            'INTEGER',
            'NOT NULL',
        ],
        [
            'inventory',
            'TEXT',
        ],
    ]

    db.create_table(new_table_name, new_table_fields)


def create_survivors_inventory_table(db, table):
    new_table_name = table

    new_table_fields = [
        [
            'id',
            'INTEGER',
            'PRIMARY KEY',
            'AUTOINCREMENT',
        ],
        [
            'survivor_id',
            'INTEGER',
            'NOT NULL',
        ],
        [
            'loot_id',
            'INTEGER',
            'NOT NULL',
        ],
        [
            'FOREIGN KEY(survivor_id) REFERENCES survivors(id)'
        ],
        [
            'FOREIGN KEY(loot_id) REFERENCES loot(id)'
        ]
    ]

    db.create_table(new_table_name, new_table_fields)


def create_loot_table(db, table):
    new_table_name = table

    new_table_fields = [
        [
            'id',
            'INTEGER',
            'PRIMARY KEY',
            'AUTOINCREMENT',
        ],
        [
            'name',
            'TEXT',
            'NOT NULL',
            'UNIQUE',
        ],
        [
            'description',
            'TEXT',
            'NOT NULL',
        ],
        [
            'health',
            'INTEGER',
            'NOT NULL',
            'DEFAULT 0',
        ],
        [
            'stamina',
            'INTEGER',
            'NOT NULL',
            'DEFAULT 0',
        ],
        [
            'weight',
            'INTEGER',
            'NOT NULL',
        ],
        [
            'volume',
            'INTEGER',
            'NOT NULL',
        ],
        [
            'type',
            'TEXT',
            'NOT NULL',
        ],
        [
            'integrity',
            'INTEGER',
            'NOT NULL',
            'DEFAULT 100',
        ],
        [
            'damage',
            'integer',
            'NOT NULL',
            'DEFAULT 0',
        ],
        [
            'range',
            'TEXT',
            'NOT NULL',
            'DEFAULT 0',
        ],
    ]

    db.create_table(new_table_name, new_table_fields)


def insert_food_into_loot_table(db, table):
    fields = ['name', 'description', 'health', 'stamina', 'type', 'weight', 'volume']

    for food in foods:
        name = foods[food]['name']
        description = foods[food]['description']

        if 'health' not in foods[food]:
            health = 0
        else:
            health = foods[food]['health']

        if 'stamina' not in foods[food]:
            stamina = 0
        else:
            stamina = foods[food]['stamina']

        loot_type = 'food'
        weight = foods[food]['weight']
        volume = foods[food]['volume']

        values = [name, description, health, stamina, loot_type, weight, volume]

        db.insert_into_table(table, fields, values, False, True)


def insert_tools_into_loot_table(db, table):
    fields = ['name', 'description', 'stamina', 'damage', 'range', 'type', 'weight', 'volume']

    for tool in tools:
        name = tools[tool]['name']
        description = tools[tool]['description']

        if 'stamina' not in tools[tool]:
            stamina = 0
        else:
            stamina = tools[tool]['stamina']

        if 'damage' not in tools[tool]:
            damage = 0
        else:
            damage = tools[tool]['damage']

        if 'range' not in tools[tool]:
            tool_range = 0
        else:
            tool_range = tools[tool]['range']

        loot_type = 'tool'
        weight = tools[tool]['weight']
        volume = tools[tool]['volume']

        values = [name, description, stamina, damage, tool_range, loot_type, weight, volume]

        db.insert_into_table(table, fields, values, False, True)


def insert_resources_into_loot_table(db, table):
    fields = ['name', 'description', 'health', 'stamina', 'type', 'weight', 'volume']

    for resource in resources:
        name = resources[resource]['name']
        description = resources[resource]['description']

        if 'health' not in resources[resource]:
            health = 0
        else:
            health = resources[resource]['health']

        if 'stamina' not in resources[resource]:
            stamina = 0
        else:
            stamina = resources[resource]['stamina']

        loot_type = 'resource'
        weight = resources[resource]['weight']
        volume = resources[resource]['volume']

        values = [name, description, health, stamina, loot_type, weight, volume]

        db.insert_into_table(table, fields, values, False, True)
