from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm


class UserRegisterForm(UserCreationForm):
    email = forms.EmailField()

    class Meta:
        model = User  # Save data to the user model
        fields = ['username', 'email', 'password1', 'password2']  # Display form fields in this order
