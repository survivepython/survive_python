import subprocess

# Run migration scripts
subprocess.call(["python", "manage.py", "migrate"])

# Start the server
subprocess.call(["python", "manage.py", "runserver"])
